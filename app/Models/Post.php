<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Post extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->hasOne("App\Models\User");
    }

    public function comments() 
    {
        return $this->hasMany("App\Models\Comment");
    }

}
