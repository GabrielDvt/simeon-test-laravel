<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\User;
use App\Models\Comment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user) {
            return redirect('login')->withErrors(['error' => "Usuário não identificado."]);
        }

        $posts = Post::get();

        foreach($posts as $post) {
            $comments = $post->comments;

            foreach($comments as $k => $comment) {
                $comment->user = $comment->user;
            }
        }
        
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Busca apenas os posts cujo último comentário tenha sido do usuário logado
     */
    public function important()
    {
        $user = Auth::user();

        if (!$user) {
            return redirect('login')->withErrors(['error' => "Usuário não identificado."]);
        }

        $posts = Post::get();

        $important_posts = [];

        foreach($posts as $post) {
            $comments = $post->comments;

            if (empty($comments))
                continue;
            
            $last_comment = $comments->last();
            
            if ($last_comment && isset($last_comment->user_id) && $last_comment->user_id === $user->id) {
                array_push($important_posts, $post);
            }
        }

        return view('posts.index')->with(['onlyImportant' => true, 'posts' => $important_posts]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required'
        ];

        $customMessages = [
            'title.required' => 'O título é obrigatório.',
            'description.required' => 'A descrição do post é obrigatória.',
        ];

        $this->validate($request, $rules, $customMessages);

        try {
            $user = Auth::user();

            if (empty($user)) {
                return redirect()->back()->withInput()->withErrors([
                    'error' => 'Não foi possível criar este post, pois o usuário não está autenticado.'
                ]);
            }
            
            $post = new Post();
            $post->title = $request->title;
            $post->description = $request->description;
            $post->user_id = $user->id;

            $post->save();

            return redirect('/posts')->with(['success' => 'Post criado com sucesso!']);
        } catch (\Exception $exc) {
            return redirect()->back()->withInput()->withErrors([
                'error' => 'Não foi possível criar este post.'
            ]);
        }
    }

    public function comment(Request $request)
    {
        if (!isset($request->comment)) {
            return response()->json(['error' => "Comentário não encontorado."]);
        }

        if (!isset($request->post_id)) {
            return response()->json(['error' => "Post não encontrado."]);
        }

        $user = Auth::user();

        if (empty($user)) {
            return response()->json(['error' => 'Usuário não encontrado.']);
        } 

        try {
            $comment = new Comment();
            $comment->description = $request->comment;
            $comment->post_id = intval($request->post_id);
            $comment->user_id = $user->id;

            $comment->save();

            return response()->json(['author_name' => $user->name, 'comment' => $request->comment]);
        } catch (\Exception $exc) {

        }
    }
}
