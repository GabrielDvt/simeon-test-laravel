<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\User;


class LoginController extends Controller
{
    public function authenticate(Request $request)
    {

        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255'
        ];

        $customMessages = [
            'email.required' => 'O e-mail é obrigatório.',
            'password.required' => 'A senha é obrigatória.',
            'email' => 'O E-mail está em um formato incorreto.'
        ];

        $this->validate($request, $rules, $customMessages);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('posts');
        } else {
            return redirect()->back()->withInput()->withErrors(['error' => 'Credenciais incorretas.']);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        
        return redirect('/auth/login');
    }

    public function signUp(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|max:255',
            'confirmPassword' => 'required|max:255',

        ];

        $customMessages = [
            'name.required' => 'O nome é obrigatório',
            'email.required' => 'O e-mail é obrigatório.',
            'password.required' => 'A senha é obrigatória.',
            'confirmPassword.required' => 'A confirmação de senha é obrigatória',
            'email' => 'O E-mail está em um formato incorreto.',
            'email.unique' => 'O E-mail inserido já está cadastrado no sistema'
        ];

        $this->validate($request, $rules, $customMessages);

        if ($request->confirmPassword !== $request->password) {
            return redirect()->back()->withInput()->withErrors(['A senha e a confirmação de senha não conferem.']);
        }

        try {
            $user = new User();
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->name =  $request->name;

            $user->save();

            $credentials = [
                'email' => $request->email,
                'password' => $request->password
            ];

            if (Auth::attempt($credentials)) {
                return redirect()->intended('posts');
            } else {
                return redirect()->withInput()->back()->withErrors(['error' => 'Não foi possível lhe redirecionar. Realize o Login.']);
            }

        } catch (\Exception $exc) {
            return redirect()->withInput()->back()->withErrors(['Ocorreu um erro ao tentar salvar o seu usuário.']);
        }

    }
}
