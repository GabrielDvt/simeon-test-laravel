<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('auth/login');
});

Route::group(['prefix' => 'auth'], function() {
    Route::get('/login', function () {
        return view('login.login');
    });
    Route::post('/login', [LoginController::class, 'authenticate']);
    Route::post('/logout', [LoginController::class, 'logout']);

    Route::get('/signup', function() {
        return view('login.signup');
    });

    Route::post('/signup', [LoginController::class, 'signup']);
});

Route::group(['prefix' => 'posts'], function() {
    Route::get('/', [PostsController::class, 'index']);
    Route::get('/create', [PostsController::class, 'create']);
    Route::post('/', [PostsController::class, 'store']);
    Route::post('/comment', [PostsController::class, 'comment']);
    Route::get('/important', [PostsController::class, 'important']);
});



