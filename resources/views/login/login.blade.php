@extends('common.main')

@section('content')

    <div class="master-container">
        <div class="container-login-title">
            <h1 class="login-title">Bem-vindo ao Post Chat</h1>
        </div>
        
        <div class="container-login">
            <div class="container-login-header">
                <h3>Login</h3>
            </div>

            <div class="container-login-body">

                <form action="{{url('/auth/login')}}" method="POST">
                    
                    {{csrf_field()}}

                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" aria-describedby="" placeholder="E-mail">
                    </div>

                    <div class="form-group form-group-password">
                        <label for="password">Senha</label>
                        <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="password" placeholder="Senha">
                    </div>

                    <div class="forgot-password">
                        <a class="signup" href="#" onClick="javascript:alert('Esta funcionalidade não estava no escopo.')"><small>Recuperar senha</small></a>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </form>

                <a class="signup" href="{{url('auth/signup')}}">Cadastrar</a>
            </div>
        </div>

        @if($errors->any()) 
            <div class="errors">
                <ul>
                @foreach ($errors->all() as $error)
                    <li><small>{{ $error }}</small></li>
                @endforeach
                </ul>
            </div>
        @endif

        </div>
@endsection
