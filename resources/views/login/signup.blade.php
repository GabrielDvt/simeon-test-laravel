@extends('common.main')

@section('content')
<div class="master-container">
    <div class="container-signup">
        <div class="container-signup-header">
            <h3>Cadastre-se</h3>
        </div>

        <div class="container-signup-body">
            <form action="{{url('/auth/signup')}}" method="POST">
            
                {{csrf_field()}}

                <div class="form-group">
                    <label for="name">Nome *</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name" aria-describedby="" placeholder="Nome">
                </div>

                <div class="form-group">
                    <label for="email">E-mail *</label>
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" aria-describedby="" placeholder="E-mail">
                </div>

                <div class="form-group">
                    <label for="password">Senha *</label>
                    <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="password" placeholder="Senha">
                </div>

                <div class="form-group">
                    <label for="password">Confirmar senha *</label>
                    <input type="password" name="confirmPassword" value="{{ old('confirmPassword') }}" class="form-control" id="password" placeholder="Confirmar senha">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Cadastrar</button>

                <div class="go-back">
                    <a class="signup" href="{{url('/auth/login')}}"><small>Voltar</small></a>
                </div>
            </form>
        </div>
    </div>

    @if($errors->any()) 
        <div class="errors">
            <ul>
            @foreach ($errors->all() as $error)
                <li><small>{{ $error }}</small></li>
            @endforeach
            </ul>
        </div>
    @endif
</div>
@endsection