@extends('common.main')

@section('header')
<script src="https://cdn.tiny.cloud/1/54l6kg8464cky53toh3g7icuo4j1wo1fopn60ab8jq4ianfn/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>
@endsection

@section('content')

<div class="master-container-home">
    <nav class="navbar navbar-light bg-dark">
        <a class="navbar-brand" href="#">POST Chat</a>
        <form method="POST" action="{{url('auth/logout')}}">
            {{csrf_field()}}
            <button type="submit" class="btn btn-link btn-logout">Logout</button>
        </form>
    </nav>

    <div class="new-post-container">
        <div class="posts-container">
            <form action="{{url('/posts')}}" method="POST">
                {{csrf_field()}}

                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" value="{{ old('title') }}" class="form-control" id="title" aria-describedby="" placeholder="Título">
                </div>

                <div class="form-group form-group-password">
                    <label for="description">Descrição</label>

                    <textarea name="description" id="mytextarea">{{old('description')}}</textarea>
                </div>
                
                <button type="submit" class="btn btn-primary btn-block btn-post">Postar!</button>
            </form>
        </div>
    </div>

    @if($errors->any()) 
        <div class="errors">
            <ul>
            @foreach ($errors->all() as $error)
                <li><small>{{ $error }}</small></li>
            @endforeach
            </ul>
        </div>
    @endif

</div>

@endsection