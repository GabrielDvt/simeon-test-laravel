@extends('common.main')

@section('content')

<div class="master-container-home">
    <nav class="navbar navbar-light bg-dark">
        <a class="navbar-brand" href="#">POST Chat</a>
        <form method="POST" action="{{url('auth/logout')}}">
            {{csrf_field()}}
            <button type="submit" class="btn btn-link btn-logout">Logout</button>
        </form>
    </nav>

    <div class="posts-container">

        @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
             {{Session::get('success')}}
            </div>
        @endif

        <div class="header-post-container">
            <a href="{{url('/posts/create')}}" class="btn btn-primary">Novo</a>

            <div>
                @if (isset($onlyImportant))
                    <a href="{{url('/posts')}}" class="btn btn-link btn-important" title="Visualizar os posts de todos os usuários"><small>Visualizar todos os posts</small></a>
                @else
                    <a href="{{url('/posts/important')}}" class="btn btn-link btn-important" title="Visualizar apenas os posts cujo último comentário foram seus"><small>Visualizar posts mais importantes</small></a>
                @endif
            </div>
            
        </div>

        <div class="posts">
            @if(isset($posts) && count($posts) > 0)
                @foreach($posts as $post)
                <div class="post">
                    <div class="post-header">
                        <span class="post-title">{{$post->title}}</span>
                    </div>
                    <div class="post-content">
                        {!!$post->description!!}
                    </div>

                    <div class="post-comments">
                        @if($post->comments)
                            @foreach($post->comments as $comment)
                                @if ($loop->last)
                                    @if(isset($comment->user) && isset($comment->user->name))
                                        <div class="post-comment">
                                            <span class="comment-author">{{$comment->user->name}}</span>
                                            &nbsp;
                                            <span class="comment">{{$comment->description}}</span>
                                            <a href="#" class="see-more-comments">...</a>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                        <div class="post-more-comments">
                            <div class="new-comment-area" data-post="{{$post->id}}" contenteditable>Escreva um comentário...</div>
                        </div>
                    </div>
                </div>
                @endforeach
            @else
                @if(isset($onlyImportant))
                    <div class="not-found-container">
                        <h3 class="not-found">Nenhum post importante</h3>
                    </div>
                    
                @else
                    <div class="not-found-container">
                        <h3 class="not-found">Nenhum post cadastrado</h3>
                    </div>
                @endif
                
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
        let elements = document.getElementsByClassName("new-comment-area");

        const removeCommentText = function() {
            if (this.innerHTML === "Escreva um comentário...") {
                this.innerHTML = "";
            }
        };

        const submitComment = function(e) {
            
            if (e.key === 'Enter' || e.keyCode === 13) {
               
                let self = this;
            
                const handleSuccess = (data) => {
                    let postCommentsElement = $(self).closest('.post-comments');

                    if (postCommentsElement[0]) {

                        if ($(postCommentsElement[0]).find('.post-comment')[0]) {
                            let authorElement = $(postCommentsElement[0]).find('.comment-author');
                            let messageElement = $(postCommentsElement[0]).find('.comment');

                            if (authorElement[0]) {
                                $(authorElement[0]).html(data.author_name);
                            }

                            if (messageElement[0]) {
                                $(messageElement[0]).html(data.comment);
                            }
                        } else {
                            $(postCommentsElement[0]).prepend(
                            `<div class="post-comment">
                                <span class="comment-author">${data.author_name}</span>
                                &nbsp;
                                <span class="comment">${data.comment}</span>
                                <a href="#" class="see-more-comments">...</a>
                            </div>`)
                           
                        }
                    }
                }

                e.preventDefault();

                $.ajax({
                    data: {
                        "_token": "{{csrf_token()}}",
                        "post_id": this.dataset.post,
                        "comment": this.innerHTML
                    },
                    method: "POST",
                    url: "{{url('/posts/comment')}}",
                    success: function(data) {
                        handleSuccess(data);
                    },
                    error: function(err) {
                        self.innerHTML = "Não foi possível publicar o seu comentário"
                    },
                    complete: function() {
                        self.innerHTML = "Escreva um comentário..."
                    }
                });
                return;
            }
        }

        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', removeCommentText, false);
            elements[i].addEventListener('keydown', submitComment, false);
        }
    </script>
@endsection