<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("posts")->insert([
            [
                "title" => "Primeiro post",
                "description" => "Descrição do primeiro post",
                "user_id" => 1
            ]
        ]);
    }
}
