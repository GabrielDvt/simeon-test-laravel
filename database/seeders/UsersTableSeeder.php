<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            [
                "name" => "Gabriel Augusto",
                "email" => "gabriel.augusto@teste.com",
                "password" => Hash::make("123321")
            ],
            [
                "name" => "Rafael Teste",
                "email" => "rafael.teste@teste.com",
                "password" => Hash::make("123321")
            ]
        ]);
    }
}
