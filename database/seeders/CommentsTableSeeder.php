<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("comments")->insert([
            [
                "description" => "Achei muito legal este post!",
                "user_id" => 1,
                "post_id" => 1,
            ],
            [
                "description" => "Mto bom!!",
                "user_id" => 2,
                "post_id" => 1,
            ]
        ]);
    }
}
